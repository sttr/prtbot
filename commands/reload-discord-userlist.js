const { SlashCommandBuilder } = require('discord.js');
const {JWT} = require('google-auth-library');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const keys = require('../prtbot_key.json');
const { token, serverId, mysql_host, mysql_username, mysql_password, mysql_database } = require('../prtbot_config.json');

/*
(async function() {

	const googleclient = new JWT({
		email: keys.client_email,
		key: keys.private_key,
		scopes: ['https://www.googleapis.com/auth/spreadsheets'],
	  });
	
	  const doc = new GoogleSpreadsheet('1-zyWJPHBtkHNtn56U6yw6A1B3zjujuLaF7G01gRsVzk', googleclient);
	  await doc.loadInfo();
	  const sheet = doc.sheetsById[277382490];

	

}());
*/


module.exports = {
	data: new SlashCommandBuilder()
	  .setName('reload-discord-userlist')
	  .setDescription('Reload all Discord users info to the Member Spreadsheet'),
	async execute(interaction, client) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

		const googleclient = new JWT({
			email: keys.client_email,
			key: keys.private_key,
			scopes: ['https://www.googleapis.com/auth/spreadsheets'],
		  });
		
		  const doc = new GoogleSpreadsheet('1-zyWJPHBtkHNtn56U6yw6A1B3zjujuLaF7G01gRsVzk', googleclient);
		  await doc.loadInfo();

		  const sheet = doc.sheetsById[277382490];

		  await sheet.clearRows();

		  let message = "User information loading...";

		  await interaction.reply({ content: message, ephemeral: true });

		  message = "\nHere you go:";

		  let addrows = "[";

		  const guild = client.guilds.resolve(serverId);
		  
			  guild.members.fetch({ force: true, timeout: 30000 })
				  .then(members => {
					  members.forEach(member => {
						const roles = member.roles.cache.map(role => role.name);
					
						let text = '{"DiscordDisplayName": "'+ member.displayName +'", "DiscordUserID": "'+ member.user.id +'", "DiscordUserName": "'+ member.user.username +'", "Roles": "'+ roles.join(', ') +'"}';

						addrows = addrows + text + ",";

					}); 
					addrows = addrows +'{}]';
//					console.log(JSON.parse(addrows));
					sheet.addRows(JSON.parse(addrows));		
				}
				).catch(console.error);

			} catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
	  }
	}
  };
  
