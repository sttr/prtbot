const { SlashCommandBuilder } = require('discord.js');
const {JWT} = require('google-auth-library');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const keys = require('../prtbot_key.json');
const { token, serverId, mysql_host, mysql_username, mysql_password, mysql_database, wix_api_key, wix_site_id } = require('../prtbot_config.json');
const http = require("https");


module.exports = {
	data: new SlashCommandBuilder()
	  .setName('test4')
	  .setDescription('Signup to test event')
	  ,
	async execute(interaction, client) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

		const guild = client.guilds.resolve(serverId);

		  let replymessage = "I will start searching for signups...";
		  await interaction.reply({ content: replymessage, ephemeral: true });

		  const options_query = {
			host: 'www.wixapis.com',
			port: 443,
			path: '/wix-data/v2/items/query',
			method: 'POST',
			headers: {
			  'Content-Type': 'application/json',
			  'Accept': 'application/json, text/plain, */*',
			  'Authorization': wix_api_key,
			  'wix-site-id': wix_site_id
			}
		  };

		  const options_insert = {
			host: 'www.wixapis.com',
			port: 443,
			path: '/wix-data/v2/items',
			method: 'POST',
			headers: {
			  'Content-Type': 'application/json',
			  'Accept': 'application/json, text/plain, */*',
			  'Authorization': wix_api_key,
			  'wix-site-id': wix_site_id
			}
		  };

		  let postData = ""; 
		  let postData_query_1 = ""; 
		  let postData_query_2 = ""; 
		  let signupmsgcnt = 0;

  async function processMessages(eventid, eventname, channelid, roleid) {
	const channel = await client.channels.cache.get(channelid);
	//console.log(roleid);
  
	try {
	  let message = await channel.messages.fetch({ limit: 1 });
	  let memberid = 0;
  
	  while (message) {
		const messagePage = await channel.messages.fetch({ limit: 100, before: message.id });
  
		for (const msg of messagePage.values()) {



		  if (msg.content === "<:Im_In:1135597389613375519>") {
			signupmsgcnt++;
			//console.log("We found "+ signupmsgcnt +" signup messages");
			memberid = 0;
  
			const postData_query_1 = JSON.stringify({
				"dataCollectionId": 'PRTMembers',
				"query": {
					"filter": {
						"discordId": msg.author.id
					},
					"paging": {
					  "limit": "1"
					}
				  }
			});
  
			try {
			  const response1 = await sendHttpRequest(options_query, postData_query_1);
			  const dataItems = JSON.parse(response1).dataItems;
  
			  for (const event of dataItems) {
				//console.log("Member information from WIX:");
				//console.log(event);
				memberid = event.data.memberID;
			  }
  
			  if (memberid > 0) {
				const postData_query_2 = JSON.stringify({
					"dataCollectionId": 'EventRegistrations',
					"query": {
						"filter": {
							"memberID": memberid
						},
						"paging": {
						"limit": "1"
						}
					}
				});
  
				try {
				  const response2 = await sendHttpRequest(options_query, postData_query_2);
				  const dataItems2 = JSON.parse(response2).dataItems;
  
				  if (dataItems2.length === 0) {
					const postData_insert = JSON.stringify({
						"dataCollectionId": 'EventRegistrations',
						"dataItem": {
							"data": {
								"method": "Discord",
								"eventID": eventid,  
								"memberID": memberid,
								"DiscordUserid": msg.author.id,
							}
						}
					});
  
					await sendHttpRequest(options_insert, postData_insert);

					const member = await guild.members.fetch(msg.author.id);

					// Add role and send a reply to the processed message
					const roleToAdd = await guild.roles.fetch(roleid);
					if (roleToAdd) {
					  await member.roles.add(roleToAdd);
					  console.log("Successfully signed up member "+ memberid +" to event "+ eventname +"");
					  //msg.reply("Your signup for "+ eventname +" has been recorded and the participant role has been added to your Discord user! Good luck and have fun in the tournament!");
					} else {
					  console.log("Role not found.");
					  //msg.reply("Your signup for "+ eventname +" has been recorded, but the participant role could not be assigned to your Discord user. Please contact Event Staff to assist you get the correct roles");
					}

				  } else {
					//msg.reply("It looks like you have already signed up :)"); <- This message will fire every time function is run, lets avoid that :D
				  }
				} catch (error) {
				  console.log("Error querying member signup:", error.message);
				}
			  }
			} catch (error) {
			  console.log("Error querying member info:", error.message);
			}
		  }
		}
  
		// Update message pointer
		message = messagePage.size > 0 ? messagePage.last() : null;
	  }
	} catch (error) {
	  console.log("Error fetching messages:", error.message);
	}
  }
  
  async function sendHttpRequest(options, postData) {
	return new Promise((resolve, reject) => {
	  const req = http.request(options, (res) => {
		let data = "";
  
		res.on("data", (chunk) => {
		  data += chunk;
		});
  
		res.on("end", () => {
		  resolve(data);
		});
	  });
  
	  req.on("error", (error) => {
		reject(error);
	  });
  
	  if (postData) {
		req.write(postData);
	  }
  
	  req.end();
	});
  }



  let message = "Querying WIX for updated list of all events...";

  const options2 = {
	host: 'www.wixapis.com',
	port: 443,
	path: '/wix-data/v2/items/query',
	method: 'POST',
	headers: {
	  'Content-Type': 'application/json',
	  'Accept': 'application/json, text/plain, */*',
	  'Authorization': wix_api_key,
	  'wix-site-id': wix_site_id
	}
  };

  
  const postData_3 = JSON.stringify({
	  "dataCollectionId": 'PRTEvents',
	  "query": {
		  "paging": {
			"limit": "150"
		  }
		}
	});
  
  var req = http.request(options2, async function(res) {
	res.setEncoding('utf8');
	try {
		const data = await new Promise((resolve, reject) => {
			let chunks = '';
			res.on('data', chunk => (chunks += chunk));
			res.on('end', () => resolve(chunks));
			res.on('error', reject);
		  });
	  
		  const json = JSON.parse(data);
		  for (const event of json.dataItems) {
	  
			  var roleid = event.data.discordRoleID;
			  var channelid = event.data.discordSignUp;
			  var eventid = event.data.eventID;
			  var eventname = event.data.eventTitle;
	  
			  if (roleid && channelid) {
				  // Call the main function to start processing messages
				  console.log("Fetching signups for "+ eventname);
//				  console.log(event);
				  await processMessages(eventid, eventname, channelid, roleid).catch((error) => {
					  console.error("An error occurred:", error.message);
				  });
			  } else {
				  console.log("The event "+ eventname +" does not have proper Discord channelid/roleid specified yet");
			  }
			};
			await interaction.followUp({ content: "All done now", ephemeral: true });
	} catch (error) {
		console.error("An error occurred:", error.message);
	}
  });
  
  req.on('error', function(e) {
	console.log('problem with request: ' + e.message);
  });
  
  req.write(postData_3);
  req.end();




























		  /*


			// Create message pointer
			let message = await channel.messages
				.fetch({ limit: 1 })
				.then(messagePage => (messagePage.size === 1 ? messagePage.at(0) : null))
			;

			let memberid = 0;
		  
			while (message) {
			  await channel.messages
				.fetch({ limit: 100, before: message.id })
				.then(messagePage => {
				  messagePage.forEach(msg => {
					postData = "";
					postData_query_1 = ""; 
					if (msg.content == "<:Im_In:1135597389613375519>") {
						console.log("We found a signup message");
						memberid = 0;
						
						// Get member info from WiX
						postData_query_1 = JSON.stringify({
							"dataCollectionId": 'PRTMembers',
							"query": {
								"filter": {
									"discordId": msg.author.id
								},
								"paging": {
								  "limit": "1"
								}
							  }
						  });

						console.log("Querying WiX for member information");
						var req = http.request(options_query, function(res) {
							res.setEncoding('utf8');
							res.on('data', function (chunk) {
								const json = JSON.parse(chunk);
								json.dataItems.forEach(event => {
									console.log("Member information from WIX:");
									console.log(event);
									memberid = event.data.memberID;
								});

								if (memberid > 0) {
									// Discord user is an valid member
	
									// Search to see if member has already signed up
									postData_query_2 = JSON.stringify({
										"dataCollectionId": 'SignupTest',
										"query": {
											"filter": {
												"title": memberid
											},
											"paging": {
											"limit": "1"
											}
										}
									});
	
									// Execute search query
									var req3 = http.request(options_query, function(res3) {
										res3.setEncoding('utf8');
										res3.on('data', function (chunk) {
											const json = JSON.parse(chunk);
											console.log(json);
											json.dataItems.forEach(event => {
												console.log("Member already signed up?");
												console.log(event);
											});
	
											if (json.dataItems.length == 0) {
	
												// Write entry to database
												postData = JSON.stringify({
													"dataCollectionId": 'SignupTest',
													"dataItem": {
														"data": {
															"test1": "Test Event 123",  
															"title": memberid,
															"DiscordUserid": msg.author.id,
														}
													}
												});
					
												var req2 = http.request(options_insert, function(res2) {
													res2.setEncoding('utf8');
												});
											
												req2.on('error', function(e) {
													console.log('problem with request: ' + e.message);
												});				
												req2.write(postData);	
												req2.end();
	
											}
	
										});
									});
									req3.on('error', function(e) {
										console.log('problem with request: ' + e.message);
									});				
									req3.write(postData_query_2);	
									req3.end();
	
								}

							});


						});
												  
						req.on('error', function(e) {
							console.log('problem with request: ' + e.message);
						});				
						req.write(postData_query_1);	
						req.end();
		
					}
				  });
				  // Update our message pointer to be the last message on the page of messages
				  message = 0 < messagePage.size ? messagePage.at(messagePage.size - 1) : null;
				});
			}
			*/
	  } catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
	  }
	}
  };
  
