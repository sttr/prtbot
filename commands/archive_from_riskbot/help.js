const { SlashCommandBuilder } = require('discord.js');
var mysql = require('mysql2');
const { mysql_host, mysql_username, mysql_password, mysql_database } = require('../config.json');

module.exports = {
	data: new SlashCommandBuilder()
	  .setName('help')
	  .setDescription('Help with using riskbot'),
	async execute(interaction) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);
		let message = "1v1 casual tournament commands:\n/1v1-result - Report the outcome of a match between two players. One match consists of 4 games with 4 total points to be given out.\n/1v1-status - See your own ranking in the tournament\n/1v1-top10 - See the leaderboard of the tournament\n/1v1-save-reslts - Check for verified results in the message log and add them to the results database\n\nAssassins bot\n/assassin - Assign assasination targets to players in a assassins-game. All players must be able to recieve DM's for this to work.";
		await interaction.reply({ content: message, ephemeral: true });

	  } catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error getting status, please try again later", ephemeral: true });
	  }
	}
  };
  
