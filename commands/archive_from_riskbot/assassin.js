const { SlashCommandBuilder } = require('discord.js');

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

module.exports = {
	data: new SlashCommandBuilder()
		.setName('assassin')
		.setDescription('Assign assassin target to specified users')
		.addUserOption(option =>
			option
			.setName('user1')
			.setDescription('user1')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user2')
			.setDescription('user2')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user3')
			.setDescription('user3')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user4')
			.setDescription('user4')
			.setRequired(false)
		)
		.addUserOption(option =>
			option
			.setName('user5')
			.setDescription('user5')
			.setRequired(false)
		)
		.addUserOption(option =>
			option
			.setName('user6')
			.setDescription('user6')
			.setRequired(false)
		)
		,
		async execute(interaction) {

			const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

			let errors = "Assasination assignment executed.";
			let users = [];
			let targets = [];

			const user1 = interaction.options.getUser('user1');
			const user2 = interaction.options.getUser('user2');
			const user3 = interaction.options.getUser('user3');
			const user4 = interaction.options.getUser('user4');
			const user5 = interaction.options.getUser('user5');
			const user6 = interaction.options.getUser('user6');

			if (user1) {
				//console.log(user1.username);
				users.push(user1);
				targets.push(user1);
			}
			if (user2) {
				if (user2.username) {
					if (!targets.includes(user2)) {
						//console.log(user2.username);
						users.push(user2);
						targets.push(user2);
					};
				}
			}
			if (user3) {
				if (user3.username) {
					if (!targets.includes(user3)) {
						//console.log(user3.username);
						users.push(user3);
						targets.push(user3);
					};
				}
			}
			if (user4) {
				if (user4.username) {
					if (!targets.includes(user4)) {
						//console.log(user4.username);
						users.push(user4);
						targets.push(user4);
					};
				}
			}
			if (user5) {
				if (user2.username) {
					if (!targets.includes(user5)) {
						//console.log(user5.username);
						users.push(user5);
						targets.push(user5);
					};
				}
			}
			if (user6) {
				if (user6.username) {
					if (!targets.includes(user6)) {
						//console.log(user6.username);
						users.push(user6);
						targets.push(user6);
					};
				}
			}


			var targets_used = [];

			if (users.length > 2) {
				for (i = 0; i < users.length; i++) {
					target = "0";
					while (target === "0") {
						var t = targets[Math.floor(Math.random()*targets.length)];
						if (t != users[i] && !targets_used.includes(t.username)) {
							target = t.username;
							targets_used.push(t.username);
						}
					}
					console.log(`Hi ${users[i].username}, I've been asked by ${interactionUser.user.username} to assign you a target for an upcoming Assassin game.\n\n Your target for the game will be **${t.username}**\n\nDiscord username may differ from other nicknames you may know them by.\n\nBest of luck!`);
					await users[i].send(`I've been asked by ${interactionUser.user.username} to assign you a target for an upcoming Assassin game.\n\n Your target for the game will be **${t.username}**\n\nDiscord username may differ from other nicknames you may know them by.\n\nBest of luck!`).catch(() => errors = errors + `\n${users[i].username} does not accept DMs. Unable to tell them their target :(`);
				}
			} else {
				errors = errors + '\n\nERROR: Not enough unique users identified, unable to assign targets to everyone';
			}
			await interaction.followUp({ content: errors, ephemeral: true });
		}
};
