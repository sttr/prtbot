const { SlashCommandBuilder } = require('discord.js');

const { token, serverId, mysql_host, mysql_username, mysql_password, mysql_database } = require('../prtbot_config.json');

module.exports = {
	data: new SlashCommandBuilder()
	  .setName('invite')
	  .setDescription('Creates an invite to the server'),
	async execute(interaction, client) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

		const guild = await client.guilds.fetch(serverId);

		let channel = await client.channels.fetch('1136031765929132196');

		let newInvite = await channel.createInvite({
			maxUses: 1, // After one use it will be void
			unique: true, // That tells the bot not to use an existing invite so that this will be unique
			maxAge: 86400 // By default invites last 24 hours. If you want to change that, modify this (0 = lasts forever, time in seconds)
		  });

		  let message = "Invite link: https://discord.gg/"+ newInvite;

		await interaction.reply({ content: message, ephemeral: true });

	  } catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
	  }
	}
  };
  
