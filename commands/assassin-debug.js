
const { SlashCommandBuilder } = require('discord.js');

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

module.exports = {
	data: new SlashCommandBuilder()
		.setName('assassin-debug')
		.setDescription('DEBUG assassin without DM')
		.addUserOption(option =>
			option
			.setName('user1')
			.setDescription('user1')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user2')
			.setDescription('user2')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user3')
			.setDescription('user3')
			.setRequired(true)
		)
		.addUserOption(option =>
			option
			.setName('user4')
			.setDescription('user4')
			.setRequired(false)
		)
		.addUserOption(option =>
			option
			.setName('user5')
			.setDescription('user5')
			.setRequired(false)
		)
		.addUserOption(option =>
			option
			.setName('user6')
			.setDescription('user6')
			.setRequired(false)
		)
		,
		async execute(interaction) {

			const interactionUser = await interaction.guild.members.fetch(interaction.user.id);


			let errors = "Assasination DEBUG executed.";
			let users = [];
			let targets = [];

			const user1 = interaction.options.getMember('user1');
			const user2 = interaction.options.getMember('user2');
			const user3 = interaction.options.getMember('user3');
			const user4 = interaction.options.getMember('user4');
			const user5 = interaction.options.getMember('user5');
			const user6 = interaction.options.getMember('user6');

			let dmtargets = "";

			if (user1) {
				//console.log(user1.username);
				users.push(user1);
				targets.push(user1);
				let name = 'globalName:'+ user1.user.globalName +', username:'+ user1.user.username +', displayname:'+ user1.displayName +', nickname:'+ user1.user.nickname;
				dmtargets = dmtargets + name + "\n";	
			}
			if (user2) {
				if (user2.user.username) {
					if (!targets.includes(user2)) {
						users.push(user2);
						targets.push(user2);
						let name = 'globalName:'+ user2.user.globalName +', username:'+ user2.user.username +', displayname:'+ user2.displayName +', nickname:'+ user2.user.nickname;
						dmtargets = dmtargets + name + "\n";	
		
					};
				}
			}
			if (user3) {
				if (user3.user.username) {
					if (!targets.includes(user3)) {
						users.push(user3);
						targets.push(user3);
						let name = 'globalName:'+ user3.user.globalName +', username:'+ user3.user.username +', displayname:'+ user3.displayName +', nickname:'+ user3.user.nickname;
						dmtargets = dmtargets + name + "\n";	
					};
				}
			}
			if (user4) {
				if (user4.user.username) {
					if (!targets.includes(user4)) {
						users.push(user4);
						targets.push(user4);
						let name = 'globalName:'+ user4.user.globalName +', username:'+ user4.user.username +', displayname:'+ user4.displayName +', nickname:'+ user4.user.nickname;
						dmtargets = dmtargets + name + "\n";	
					};
				}
			}
			if (user5) {
				if (user2.user.username) {
					if (!targets.includes(user5)) {
						users.push(user5);
						targets.push(user5);
						let name = 'globalName:'+ user5.user.globalName +', username:'+ user5.user.username +', displayname:'+ user5.displayName +', nickname:'+ user5.user.nickname;
						dmtargets = dmtargets + name + "\n";	


					};
				}
			}
			if (user6) {
				if (user6.user.username) {
					if (!targets.includes(user6)) {
						users.push(user6);
						targets.push(user6);
						let name = 'globalName:'+ user6.user.globalName +', username:'+ user6.user.username +', displayname:'+ user6.displayName +', nickname:'+ user6.user.nickname;
						dmtargets = dmtargets + name + "\n";	
					};
				}
			}

			await interaction.reply({ content: "Here are the debug stuff:\n"+ dmtargets, ephemeral: true });
		}
};
