const { SlashCommandBuilder } = require('discord.js');
const {JWT} = require('google-auth-library');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const keys = require('../prtbot_key.json');
const { token, serverId, mysql_host, mysql_username, mysql_password, mysql_database, wix_api_key, wix_site_id } = require('../prtbot_config.json');

const http = require("https");



/*
(async function() {

	const googleclient = new JWT({
		email: keys.client_email,
		key: keys.private_key,
		scopes: ['https://www.googleapis.com/auth/spreadsheets'],
	  });
	
	  const doc = new GoogleSpreadsheet('1-zyWJPHBtkHNtn56U6yw6A1B3zjujuLaF7G01gRsVzk', googleclient);
	  await doc.loadInfo();
	  const sheet = doc.sheetsById[277382490];

	

}());
*/


module.exports = {
	data: new SlashCommandBuilder()
	  .setName('test3')
	  .setDescription('Gets event list')
	  ,
	async execute(interaction, client) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

		  let message = "Querying WIX for updated list of all events...";

		  const options2 = {
			host: 'www.wixapis.com',
			port: 443,
			path: '/wix-data/v2/items/query',
			method: 'POST',
			headers: {
			  'Content-Type': 'application/json',
			  'Accept': 'application/json, text/plain, */*',
			  'Authorization': wix_api_key,
			  'wix-site-id': wix_site_id
			}
		  };

		  
		  const postData = JSON.stringify({
			  "dataCollectionId": 'EventRegistrations',
			  "query": {
				  "paging": {
					"limit": "150"
				  }
				}
			});
		  
		  let followup = "I found the following PRT events:\n";

		  var req = http.request(options2, function(res) {
			res.setEncoding('utf8');
			res.on('data', function (chunk) {
			  const json = JSON.parse(chunk);
			  json.dataItems.forEach(event => {
				console.log(event);
			  	followup = followup + "\n" + event.data.eventID;
			  });
			  interaction.followUp({ content: followup, ephemeral: true });
			});
		  });
		  
		  req.on('error', function(e) {
			console.log('problem with request: ' + e.message);
		  });
		  
		  req.write(postData);
		  req.end();
		  

		  await interaction.reply({ content: message, ephemeral: true });

		  message = "\nHere you go:";

		  // GET Member info
		  /*
		  const sheet = doc.sheetsById[1500865626];
		  const rows = await sheet.getRows();
		  for (i = 0; i < rows.length; i++) {
			console.log(rows[i]);
		  }
		  */
//		let message = "Spreadsheet title: "+ doc.title +", and on the sheet MembersUpdated there are "+ sheet.rowCount +" rows in total.";

	  } catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
	  }
	}
  };
  
