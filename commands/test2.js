const { SlashCommandBuilder } = require('discord.js');
const {JWT} = require('google-auth-library');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const keys = require('../prtbot_key.json');
const { token, serverId, mysql_host, mysql_username, mysql_password, mysql_database } = require('../prtbot_config.json');

/*
(async function() {

	const googleclient = new JWT({
		email: keys.client_email,
		key: keys.private_key,
		scopes: ['https://www.googleapis.com/auth/spreadsheets'],
	  });
	
	  const doc = new GoogleSpreadsheet('1-zyWJPHBtkHNtn56U6yw6A1B3zjujuLaF7G01gRsVzk', googleclient);
	  await doc.loadInfo();
	  const sheet = doc.sheetsById[277382490];

	

}());
*/


module.exports = {
	data: new SlashCommandBuilder()
	  .setName('test2')
	  .setDescription('Gets some data from google spreasheet'),
	async execute(interaction, client) {
	  try {
		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);

		const googleclient = new JWT({
			email: keys.client_email,
			key: keys.private_key,
			scopes: ['https://www.googleapis.com/auth/spreadsheets'],
		  });
		
		  const doc = new GoogleSpreadsheet('1-zyWJPHBtkHNtn56U6yw6A1B3zjujuLaF7G01gRsVzk', googleclient);
		  await doc.loadInfo();

		  const sheet = doc.sheetsById[277382490];

		  let message = "User information loading...";

		  await interaction.reply({ content: message, ephemeral: true });

		  message = "\nHere you go:";

		  // GET Member info
		  /*
		  const sheet = doc.sheetsById[1500865626];
		  const rows = await sheet.getRows();
		  for (i = 0; i < rows.length; i++) {
			console.log(rows[i]);
		  }
		  */
//		let message = "Spreadsheet title: "+ doc.title +", and on the sheet MembersUpdated there are "+ sheet.rowCount +" rows in total.";

	  } catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
	  }
	}
  };
  
