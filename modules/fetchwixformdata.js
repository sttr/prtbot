const http = require('http');
const { wix_api_key, wix_site_id } = require('../prtbot_config.json');

async function sendHttpRequest(options) {
    return new Promise((resolve, reject) => {
      const req = http.request(options, (res) => {
          let data = "";

          res.on("data", (chunk) => {
              data += chunk;
          });

          res.on("end", () => {
              resolve(data);
          });
      });

      req.on("error", (error) => {
          reject(error);
      });



      req.end();
    });
}

async function wixFormFetcher(submissionId) {
    const options_query = {
        host: 'www.wixapis.com',
        port: 80,
        path: `/_api/form-submission-service/v4/submissions/${submissionId}`,
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': wix_api_key,
        'wix-site-id': wix_site_id,
        },
    };

    try {
        const res = await sendHttpRequest(options_query);
        //console.log(res);
        const response = JSON.parse(res);
        return response;
    } catch (error) {

        throw new Error(`Error fetching Wix form data: ${error.message}`);

    }
}

module.exports = {
    wixFormFetcher,
};