const http = require('https');
const { serverId, wix_api_key, wix_site_id, logchannelid } = require('../prtbot_config.json');

// Check for new members and post a message in staff log channel

const options_query = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items/query',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  const options_insert = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };


  async function lognewmember(client, member) {

    const options_update = {
        host: 'www.wixapis.com',
        port: 443,
        path: `/wix-data/v2/items/${member.id}`,
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json, text/plain, */*',
          'Authorization': wix_api_key,
          'wix-site-id': wix_site_id
        }
      };

      const guild = await client.guilds.resolve(serverId);

              try {


                  if (member.data.id > 250) {
                    const channel = await guild.channels.fetch(logchannelid);
                    const user = await guild.members.fetch(member.data.DiscordUserid);
                    let text = `New ${member.data.status} member registered: ${member.data.riskName} (Discord user: ${user})`;
                    let message = await channel.send(text);  
                  }

                    let postData_update = {
                      "dataCollectionId": 'PRTMembers',
                      "dataItem": {
                          "data": member.data
                      }
                    };
                    postData_update.dataItem.data.alert = "Alerted";
                    postData_update = JSON.stringify(postData_update);

                    await sendHttpRequest(options_update, postData_update);

              } catch (error) {
                  console.log("Error querying member signup:", error.message);
              }

  }

  async function sendHttpRequest(options, postData) {
      return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            let data = "";

            res.on("data", (chunk) => {
                data += chunk;
            });

            res.on("end", () => {
                resolve(data);
            });
        });

        req.on("error", (error) => {
            reject(error);
        });

        if (postData) {
            req.write(postData);
        }

        req.end();
      });
  }

  const postData_3 = JSON.stringify({
    "dataCollectionId": 'PRTMembers',
    "query": {
        "filter": {
            "alert": null
            },
        "paging": {
            "limit": "50"
        }
    }
});

  async function lognewmembers(client) {
      try {
        const response = await sendHttpRequest(options_query, postData_3);
        const json = JSON.parse(response);
    
        for (let member of json.dataItems) {
              await lognewmember(client, member);
        }
        console.log("Done looping through new members");
      } catch (error) {
        console.error("An error occurred while fetching data and processing:", error.message);
      }
    }

module.exports = {
    lognewmembers
};