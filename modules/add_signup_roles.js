const http = require('https');
const { serverId, wix_api_key, wix_site_id } = require('../prtbot_config.json');

// Check for new event signups every hour
const options_query = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items/query',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  const options_insert = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };


  async function processSignup(client, memberid, discorduserid, roleid, itemid, signup) {

    const options_update = {
        host: 'www.wixapis.com',
        port: 443,
        path: `/wix-data/v2/items/${itemid}`,
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json, text/plain, */*',
          'Authorization': wix_api_key,
          'wix-site-id': wix_site_id
        }
      };

      const guild = await client.guilds.resolve(serverId);

              try {

                  const member = await guild.members.fetch(discorduserid);

                  // Add role and send a reply to the processed message
                  const roleToAdd = await guild.roles.fetch(roleid);
                  if (roleToAdd) {
                      await member.roles.add(roleToAdd);
                      console.log("Successfully added role "+ roleid +" "+ memberid +"");

                        let postData_update = {
                            "dataCollectionId": 'EventRegistrations',
                            "dataItem": {
                                "data": signup.data
                            }
                        };
                    postData_update.dataItem.data.discordRole = true;
                    postData_update = JSON.stringify(postData_update);
  
                    await sendHttpRequest(options_update, postData_update);

                  } else {
                      console.log("Role not found.");
                  }

              } catch (error) {
                  console.log("Error querying member signup:", error.message);
              }

  }

  async function sendHttpRequest(options, postData) {
      return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            let data = "";

            res.on("data", (chunk) => {
                data += chunk;
            });

            res.on("end", () => {
                resolve(data);
            });
        });

        req.on("error", (error) => {
            reject(error);
        });

        if (postData) {
            console.log(postData);
            req.write(postData);
        }

        req.end();
      });
  }

  const postData_3 = JSON.stringify({
    "dataCollectionId": 'EventRegistrations',
    "query": {
        "filter": {
            "method": "Web",
            "discordRole": null
            },
        "paging": {
            "limit": "150"
        }
    }
});

  async function addSignupRoles(client, allEvents) {
      try {
        const response = await sendHttpRequest(options_query, postData_3);
        const json = JSON.parse(response);
    
        for (let signup of json.dataItems) {

            if (allEvents[signup.data.eventID].discordRoleID && signup.data.DiscordUserid) {
                await processSignup(client, signup.data.memberID, signup.data.DiscordUserid, allEvents[signup.data.eventID].discordRoleID, signup.id, signup);
            } else {
                console.log("Unable to find event discordRoleID or DiscordUserid in EventRegistrations");
            }
        }
        console.log("Done looping through signups without roles");
      } catch (error) {
        console.error("An error occurred while fetching data and processing:", error.message);
      }
    }

module.exports = {
    addSignupRoles
};