const http = require('https');

async function changeNickname(client, serverId, userid, newnick) {
	const guild = await client.guilds.resolve(serverId);
	const member = await guild.members.fetch(userid);
	await member.setNickname(newnick);
}

module.exports = {
    changeNickname
};