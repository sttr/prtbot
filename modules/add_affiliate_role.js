const http = require('https');
const { serverId, wix_api_key, wix_site_id } = require('../prtbot_config.json');
const { wixDataFetcher } = require('../modules/fetchwixcollection.js');

const options_query = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items/query',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  const options_insert = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  async function sendHttpRequest(options, postData) {
      return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            let data = "";

            res.on("data", (chunk) => {
                data += chunk;
            });

            res.on("end", () => {
                resolve(data);
            });
        });

        req.on("error", (error) => {
            reject(error);
        });

        if (postData) {
            console.log(postData);
            req.write(postData);
        }

        req.end();
      });
  }

  const postData_3 = JSON.stringify({
    "dataCollectionId": 'PRTMembers',
    "query": {
        "filter": {
            "alert": null
            },
        "paging": {
            "limit": "1"
        }
    }
});

async function getAllAffiliates(client) {
	console.log("getAllAffiliates triggeres");
	const requestAllAffiliates = JSON.stringify({
		"dataCollectionId": 'PRTMembers',
		"query": {
			"filter": {
				"status": "Affiliate",
                "discord": null
			},
			"paging": {
				"limit": "400"
			}
		}
	});
	try {
		const allAffiliates = [];
		const allAffiliatesRes = await wixDataFetcher(requestAllAffiliates);
		const guild = await client.guilds.resolve(serverId);
		// Process the collectionData and create objects as needed
        for (const member of allAffiliatesRes) {
			console.log("Processing affiliate "+ member.data.discordName);
			try {
				const user = await guild.members.fetch(member.data.discordId);
				const roleToAdd = await guild.roles.fetch('1205848110941741087');
				if (roleToAdd) {
					await user.roles.add(roleToAdd);


                    const options_update = {
                        host: 'www.wixapis.com',
                        port: 443,
                        path: `/wix-data/v2/items/${member.id}`,
                        method: 'PUT',
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept': 'application/json, text/plain, */*',
                          'Authorization': wix_api_key,
                          'wix-site-id': wix_site_id
                        }
                      };
                
                
                      let postData_update = {
                        "dataCollectionId": 'PRTMembers',
                        "dataItem": {
                            "data": member.data
                        }
                      };
                      postData_update.dataItem.data.discord = true;
                      postData_update = JSON.stringify(postData_update);
  
                      await sendHttpRequest(options_update, postData_update);
  



					console.log("Successfully added role Affiliate to "+ member.data.discordName +"");
				}	
			} catch (error) {
				console.error(error.message);
			}
		}
	} catch (error) {
		console.log("Something went wrong when trying to add roles");
		console.error(error.message);
	}
}


module.exports = {
    getAllAffiliates
};