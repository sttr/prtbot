const http = require('https');
const { serverId, wix_api_key, wix_site_id } = require('../prtbot_config.json');

// Check for new event signups every hour
const options_query = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items/query',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  const options_insert = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };

  async function processMessages(client, eventid, eventname, channelid, roleid, lastmsg) {

      const guild = await client.guilds.resolve(serverId);
      const channel = await guild.channels.fetch(channelid);

      console.log("Scanning through all messages in "+ channel.name);

      try {

        let message = null;

        if (lastmsg > 0) {
            message = await channel.messages.fetch(lastmsg);
        } else {
            message = await channel.messages.fetch({limit: 1, after: 0});
        }
        
        let memberid = 0;

        while (message) {
            const messagePage = await channel.messages.fetch({ limit: 100, after: message.id });

            for (const msg of messagePage.values()) {

                if (msg.content === "<:Im_In:1135597389613375519>") {
                    memberid = 0;

                    const postData_query_1 = JSON.stringify({
                        "dataCollectionId": 'PRTMembers',
                        "query": {
                            "filter": {
                                "discordId": msg.author.id
                            },
                            "paging": {
                                "limit": "1"
                            }
                        }
                    });

                    try {
                        const response1 = await sendHttpRequest(options_query, postData_query_1);
                        const dataItems = JSON.parse(response1).dataItems;

                        for (const event of dataItems) {
                            //console.log("Member information from WIX:");
                            //console.log(event);
                            memberid = event.data.memberID;
                        }

                        if (memberid > 0) {
                            const postData_query_2 = JSON.stringify({
                                "dataCollectionId": 'EventRegistrations',
                                "query": {
                                    "filter": {
                                        "memberID": memberid,
                                        "eventID": eventid,  
                                    },
                                    "paging": {
                                    "limit": "1"
                                    }
                                }
                            });

                            try {
                                const response2 = await sendHttpRequest(options_query, postData_query_2);
                                const dataItems2 = JSON.parse(response2).dataItems;

                                if (dataItems2.length === 0) {
                                    const postData_insert = JSON.stringify({
                                        "dataCollectionId": 'EventRegistrations',
                                        "dataItem": {
                                            "data": {
                                                "method": "Discord",
                                                "eventStatus": "Participant",
                                                "eventID": eventid,  
                                                "memberID": memberid,
                                                "DiscordUserid": msg.author.id,
                                            }
                                        }
                                    });

                                    await sendHttpRequest(options_insert, postData_insert);

                                    const member = await guild.members.fetch(msg.author.id);

                                    // Add role and send a reply to the processed message
                                    const roleToAdd = await guild.roles.fetch(roleid);
                                    if (roleToAdd) {
                                        await member.roles.add(roleToAdd);
                                        //console.log("Successfully signed up member "+ memberid +" to event "+ eventname +"");
                                        msg.reply("Your signup for "+ eventname +" has been recorded and the participant role has been added to your Discord user! Good luck and have fun in the tournament!");
                                    } else {
                                        //console.log("Role not found.");
                                        msg.reply("Your signup for "+ eventname +" has been recorded, but the participant role could not be assigned to your Discord user. Please contact Event Staff to assist you get the correct roles");
                                    }

                                } else {
                                    console.log("Member has already signed up...");
                                    //msg.reply("It looks like you have already signed up :)");
                                }
                            } catch (error) {
                                console.log("Error querying member signup for message "+ msg.id +" :", error.message);
                            }
                        } else {
                            msg.reply("I was unable to sign you up for "+ eventname +" since I cannot find  your membership information. Maybe the discord user ID is different from what you have signed up with on the webpage? Please contact Event Staff to assist you get the correct roles and get you signed up for the event.");
                            console.log("Unable to find memberinfo for messageId "+ msg.id);
                        }
                    } catch (error) {
                        console.log("Error querying member info for message "+ msg.id +":", error.message);
                        //msg.reply("I was unable to sign you up for "+ eventname +" since I cannot find  your membership information. Maybe the discord user ID is different from what you have signed up with on the webpage? Please contact Event Staff to assist you get the correct roles and get you signed up for the event.");
                        console.log(response1);
                    }
                } else if (msg.content) {
                    //console.log("Invalid message: "+ msg.content);
                }

                if (msg.id > lastmsg) {
                    lastmsg = msg.id;
                }
            }

            // Update message pointer
            message = messagePage.size > 0 ? messagePage.first() : null;
          }
          return lastmsg;
      } catch (error) {
         console.log("Error fetching messages:", error.message);
         return null;
      }
  }

  async function sendHttpRequest(options, postData) {
      return new Promise((resolve, reject) => {
      const req = http.request(options, (res) => {
      let data = "";

      res.on("data", (chunk) => {
          data += chunk;
      });

      res.on("end", () => {
          resolve(data);
      });
      });

      req.on("error", (error) => {
      reject(error);
      });

      if (postData) {
      req.write(postData);
      }

      req.end();
      });
  }

  const options2 = {
  host: 'www.wixapis.com',
  port: 443,
  path: '/wix-data/v2/items/query',
  method: 'POST',
  headers: {
  'Content-Type': 'application/json',
  'Accept': 'application/json, text/plain, */*',
  'Authorization': wix_api_key,
  'wix-site-id': wix_site_id
  }
  };

  const today = new Date().toISOString();
  const postData_3 = JSON.stringify({
  "dataCollectionId": 'PRTEvents',
  "query": {
    "filter": {
    "registrationEnd": {
        "$gt": { "$date": today }
      },
    },
      "paging": {
      "limit": "150"
      }
  }
  });

  async function getSignups(client) {
      try {
        const response = await sendHttpRequest(options2, postData_3);
        const json = JSON.parse(response);
    
        for (const event of json.dataItems) {
            const eventid = event.data.eventID;
            if (eventid != "") {
                //console.log(event);
                const roleid = event.data.discordRoleID;
                const channelid = event.data.discordSignUp;
                const eventname = event.data.eventTitle;
                let lastmsg = event.data.lastDiscordSignupMsgId;
                console.log("Last message to be processed:");
                console.log(lastmsg);
                if (!lastmsg) {
                    lastmsg = 0;
                }
                //console.log(lastmsg);
                if (roleid && channelid) {

                    console.log("Fetching signups for " + eventname + " / "+ eventid + " / "+ channelid);

                    lastmsg = await processMessages(client, eventid, eventname, channelid, roleid, lastmsg);

                    const options_update = {
                        host: 'www.wixapis.com',
                        port: 443,
                        path: `/wix-data/v2/items/${event.id}`,
                        method: 'PUT',
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept': 'application/json, text/plain, */*',
                          'Authorization': wix_api_key,
                          'wix-site-id': wix_site_id
                        }
                      };

                      let postData_update = {
                        "dataCollectionId": 'PRTEvents',
                        "dataItem": {
                            "data": event.data
                        }
                    };
                    postData_update.dataItem.data.lastDiscordSignupMsgId = lastmsg;
                    postData_update = JSON.stringify(postData_update);

                    await sendHttpRequest(options_update, postData_update);

                } else {
                    console.log("The event " + eventname + " does not have proper Discord channelid/roleid specified yet");
                }
                await new Promise(resolve => setTimeout(resolve, 1000));        
            }
        }
        console.log("Done looping through all event signup channels");
      } catch (error) {
        console.error("An error occurred while fetching data and processing:", error.message);
      }
    }

module.exports = {
    getSignups
};