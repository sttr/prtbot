const http = require('https');
const { serverId, wix_api_key, wix_site_id } = require('../prtbot_config.json');

// Check for new event signups every hour
const options_query = {
    host: 'www.wixapis.com',
    port: 443,
    path: '/wix-data/v2/items/query',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/plain, */*',
      'Authorization': wix_api_key,
      'wix-site-id': wix_site_id
    }
  };


  async function sendDM(client, discorduserid, message) {

      const guild = await client.guilds.resolve(serverId);

              try {

                /*
                  const member = await guild.members.fetch(discorduserid);

    if (member.user.username == "jjbruhh_" || member.user.username == "eg_sneaks" || member.user.username == "mittyz" || member.user.username == "arcorisk" || member.user.username == "benwerkschuw" || member.user.username == "multivac0420" || member.user.username == "amoobao" || member.user.username == "qlwl" || member.user.username == "truthpowers") {
      console.log("Sending DM to "+ member.user.username +": "+ message);
      try {
        member.send(message);
      } catch (error) {
        console.log(error);
      }

    }
*/

              } catch (error) {
                  console.log("Error DMing member", error.message);
              }

  }

  async function sendHttpRequest(options, postData) {
      return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            let data = "";

            res.on("data", (chunk) => {
                data += chunk;
            });

            res.on("end", () => {
                resolve(data);
            });
        });

        req.on("error", (error) => {
            reject(error);
        });

        if (postData) {
            console.log(postData);
            req.write(postData);
        }

        req.end();
      });
  }

  const postData_3 = JSON.stringify({
    "dataCollectionId": 'EventRegistrations',
    "query": {
        "filter": {
            "eventID": "23HBWT", 
            },
        "paging": {
            "limit": "150"
        }
    }
});

  async function sendParticipantDM(client, allEvents) {
      try {
        console.log("Look for participants");
        const response = await sendHttpRequest(options_query, postData_3);
        const json = JSON.parse(response);
    
        for (let signup of json.dataItems) {

            if (allEvents[signup.data.eventID].discordRoleID && signup.data.DiscordUserid) {
                await sendDM(client, signup.data.DiscordUserid, "Thanks for taking part in the Hot Bunz World Tour! We’d love to hear your feedback, so we can assure our future events are of the highest quality.\n\nPlease fill out the form here: https://tinyurl.com/HBWTfeedback");
            } else {
                console.log("Unable to find event discordRoleID or DiscordUserid in EventRegistrations");
            }
        }
        console.log("Done looping through signups without roles");
      } catch (error) {
        console.error("An error occurred while fetching data and processing:", error.message);
      }
    }

module.exports = {
  sendParticipantDM
};