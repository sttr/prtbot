// Load required modules and config
const fs = require('node:fs');
const path = require('node:path');
const cron = require("node-cron");
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');
const { token, serverId, serverIdrwc } = require('./prtbot_config.json');
const { getSignups } = require('./modules/fetch_discord_signups.js');
const { addSignupRoles } = require('./modules/add_signup_roles.js');
const { wixDataFetcher } = require('./modules/fetchwixcollection.js');
const { wixSubFetcher } = require('./modules/fetchwixsubscriptions.js');
const { wixFormFetcher } = require('./modules/fetchwixformdata.js');
const { sendParticipantDM } = require('./modules/dm_participants.js');
const { lognewmembers } = require('./modules/new_member_log.js');
const { getAllAffiliates } = require('./modules/add_affiliate_role.js');
const { changeNickname } = require('./modules/discord_user_handler.js');



// Spawn Discord client
const client = new Client({ intents: [
	GatewayIntentBits.Guilds,
	GatewayIntentBits.GuildMessages,
	GatewayIntentBits.MessageContent,
	GatewayIntentBits.GuildMembers,
]});
client.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));


// Load slash-commands
for (const file of commandFiles) {
	const filePath = path.join(commandsPath, file);
	const command = require(filePath);
	client.commands.set(command.data.name, command);
}

async function getAllEvents() {
	console.log("All events triggeres");
	const requestAllEvents = JSON.stringify({
		"dataCollectionId": 'PRTEvents',
		"query": {
			"paging": {
				"limit": "150"
			}
		}
	});
	try {
		const allEvents = [];
		const allEventsRes = await wixDataFetcher(requestAllEvents);
		// Process the collectionData and create objects as needed
        for (const event of allEventsRes) {
			allEvents[event.data.eventID] = {};
			allEvents[event.data.eventID].eventTitle = event.data.eventTitle;
			allEvents[event.data.eventID].eventSubtitle = event.data.eventSubtitle;
			allEvents[event.data.eventID].eventEnd = event.data.eventEnd;
			allEvents[event.data.eventID].eventDescription = event.data.eventDescription;
			allEvents[event.data.eventID].eventColor = event.data.eventColor;
			allEvents[event.data.eventID].eventStart = event.data.eventStart;
			allEvents[event.data.eventID].eventType = event.data.eventType;
			allEvents[event.data.eventID].eventDirector = event.data.eventDirector;
			allEvents[event.data.eventID].discordRoleID = event.data.discordRoleID;
			allEvents[event.data.eventID].discordSignUp = event.data.discordSignUp;

		}

		addSignupRoles(client, allEvents);


	} catch (error) {
		console.error(error.message);
	}
}




async function getAllSubscriptions() {
	console.log("All subscriptions triggeres");
	const requestAllSubscriptions = JSON.stringify({
		
	});
	try {
		const allSubs = [];
		const allSubsRes = await wixSubFetcher(requestAllSubscriptions);
		// Process the collectionData and create objects as needed
		let cnt = 0;
        for (const sub of allSubsRes.orders) {
			cnt++;
			if (cnt == 1) {
				console.log(sub);
				console.log(sub.formData);
				const formData = await wixFormFetcher(sub.formData.submissionId);
				console.log(formData);	
			}
		}
		//console.log(allEvents);

	} catch (error) {
		console.error(error.message);
	}
}


// Verify to console that everything is ready
client.once(Events.ClientReady, () => {
	console.log('Hello! I am');
	console.log('_____  _____ ________ _           _   ');
	console.log('|  __ \\|  __ \\__   __| |         | |  ');
	console.log('| |__) | |__) | | |  | |__   ___ | |_ ');
	console.log('|  ___/|  _  /  | |  | `_ \\ / _ \\| __|');
	console.log('| |    | | \\ \\  | |  | |_) | (_) | |_ ');
	console.log('|_|    |_|  \\_\\ |_|  |_.__/ \\___/ \\__|');
	console.log('');
	console.log('Developed by eirik for Professional Risk Takers');
	console.log('Free to use until 2024-12-31');
	console.log('');
	console.log('Ready for action!');

	//getSignups(client)
	//getAllAffiliates(client);
	//getAllEvents();
	//lognewmembers(client);
	//changeNickname(client, serverId, '796768778977148938', 'eirik');
	
});


// Every 30 minutes
cron.schedule("0 */30 * * * *", async function () {
	await getAllEvents();
	await getSignups(client);
});

// 5 minute past every hour
cron.schedule("0 5 * * * *", async function () {
	await getAllAffiliates(client);
	await lognewmembers(client);

});


// Listen for any commands that we know
client.on(Events.InteractionCreate, async interaction => {
	if (!interaction.isChatInputCommand()) return;
	const command = client.commands.get(interaction.commandName);
	if (!command) return;
	try {
		//await interaction.reply({ content: 'I am on it!', ephemeral: true });
		await command.execute(interaction, client);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
	}

});

//client.on("debug", console.log);
client.login(token);
